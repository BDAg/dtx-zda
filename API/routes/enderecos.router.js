const express = require('express');
const router = express.Router();
var enderecosController = require('../controllers/enderecos.controller');


router.route('/')
    .post(enderecosController.post)
    .put(enderecosController.put);

router.route('/:id')
    .get(enderecosController.get)
    .delete(enderecosController.remove);

module.exports = router;