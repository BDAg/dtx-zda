const express = require('express');
const router = express.Router();
var receitasController = require('../controllers/receitas.controller');


router.route('/')
    .post(receitasController.post)
    .put(receitasController.put);

router.route('/:id')
    .get(receitasController.get)
    .delete(receitasController.remove);

module.exports = router;