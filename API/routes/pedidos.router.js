const express = require('express');
const router = express.Router();
var pedidosController = require('../controllers/pedidos.controller');


router.route('/')
    .post(pedidosController.post)
    .put(pedidosController.put);

router.route('/:id')
    .get(pedidosController.get)
    .delete(pedidosController.remove);

module.exports = router;