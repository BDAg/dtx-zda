const express = require('express');
const router = express.Router();
var produtosController = require('../controllers/produtos.controller');


router.route('/')
    .post(produtosController.post)
    .put(produtosController.put);

router.route('/:id')
    .get(produtosController.get)
    .delete(produtosController.remove);

module.exports = router;