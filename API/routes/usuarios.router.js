const express = require('express');
const router = express.Router();
var usuariosController = require('../controllers/usuarios.controller');


router.route('/')
    .post(usuariosController.post)
    .put(usuariosController.put);

router.route('/:id')
    .get(usuariosController.get)
    .delete(usuariosController.remove);

module.exports = router;