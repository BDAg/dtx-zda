var express = require('express');
var bodyParser = require('body-parser');
const cors = require('cors');

app = express();

app.use(cors());
app.use(express.json());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());




var usuariosRoutes = require('./routes/usuarios.router');
app.use('/usuarios', usuariosRoutes);

var receitasRoutes = require('./routes/receitas.router');
app.use('/receitas', receitasRoutes);

var produtosRoutes = require('./routes/produtos.router');
app.use('/produtos', produtosRoutes);

var pedidosRoutes = require('./routes/pedidos.router');
app.use('/pedidos', pedidosRoutes);

var enderecosRoutes = require('./routes/enderecos.router');
app.use('/enderecos', enderecosRoutes);




var port = '3000';

app.set('port', process.env.PORT || port);
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
});

module.exports = app;