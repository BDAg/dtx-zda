const db = require('../bin/db');

create = function create(user, result) {
    db.query("insert into usuarios set ?", user, function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};
find = function find(id, result) {
    db.query("select * from usuarios where idusuarios = ? ", id, function (err, res) {
        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};
findAll = function findAll(result) {
    db.query("select * from usuarios", function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};
update = function update(id, user, result) {
    db.query("update usuarios set ? where idusuarios = ?", [user, id], function (err, res) {
        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};
remove = function (id, result) {
    db.query("delete from usuarios where idusuarios = ?", [id], function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};

module.exports = {
    create,
    find,
    findAll,
    remove,
    update
}