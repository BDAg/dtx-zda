const db = require('../bin/db');

create = function create(user, result) {
    db.query("insert into receitas set ?", user, function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};
find = function find(id, result) {
    db.query("select * from receitas where idreceitas = ? ", id, function (err, res) {
        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};
findAll = function findAll(result) {
    db.query("select * from receitas", function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};
update = function update(id, user, result) {
    db.query("update receitas set ? where idreceitas = ?", [user, id], function (err, res) {
        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};
remove = function (id, result) {
    db.query("delete from receitas where idreceitas = ?", [id], function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};

module.exports = {
    create,
    find,
    findAll,
    remove,
    update
}