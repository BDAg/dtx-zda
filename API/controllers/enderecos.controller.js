const Enderecos = require('../models/enderecos.model');

get = function (req, res) {
    const id = req.params.id;

    if (!id) {
        return res.status(400).send({ error: true, message: 'Id não informado' });
    }

    Enderecos.find(id, res);

};

post = function (req, res) {
    let user = req.body;

    if (!user) {
        return res.status(400).send({ error: true, message: 'Objeto não informado' });
    }

    Enderecos.create(user, res);
};

put = function (req, res) {
    let user = req.body;

    if (!user) {
        return res.status(400).send({ error: user, message: 'Objeto não informado' });
    }

    Enderecos.update(user, res);
};

remove = function (req, res) {

    let id = req.params.id;

    if (!id) {
        return res.status(400).send({ error: true, message: 'Id não informado' });
    }

    Enderecos.remove(id, res);

};

module.exports = {
    get,
    post,
    put,
    remove
}