const Usuarios = require('../models/usuarios.model');

get = function (req, res) {
    const id = req.params.id;

    if (!id) {
        return res.status(400).send({ error: true, message: 'Id não informado' });
    }

    Usuarios.find(id, res);

};

post = function (req, res) {
    let user = req.body;

    if (!user) {
        return res.status(400).send({ error: true, message: 'Objeto não informado' });
    }

    Usuarios.create(user, res);
};

put = function (req, res) {
    let user = req.body;

    if (!user) {
        return res.status(400).send({ error: user, message: 'Objeto não informado' });
    }

    Usuarios.update(user, res);
};

remove = function (req, res) {

    let id = req.params.id;

    if (!id) {
        return res.status(400).send({ error: true, message: 'Id não informado' });
    }

    Usuarios.remove(id, res);

};

module.exports = {
    get,
    post,
    put,
    remove
}