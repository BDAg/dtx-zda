const Produtos = require('../models/produtos.model');

get = function (req, res) {
    const id = req.params.id;

    if (!id) {
        return res.status(400).send({ error: true, message: 'Id não informado' });
    }

    Produtos.find(id, res);

};

post = function (req, res) {
    let user = req.body;

    if (!user) {
        return res.status(400).send({ error: true, message: 'Objeto não informado' });
    }

    Produtos.create(user, res);
};

put = function (req, res) {
    let user = req.body;

    if (!user) {
        return res.status(400).send({ error: user, message: 'Objeto não informado' });
    }

    Produtos.update(user, res);
};

remove = function (req, res) {

    let id = req.params.id;

    if (!id) {
        return res.status(400).send({ error: true, message: 'Id não informado' });
    }

    Produtos.remove(id, res);

};

module.exports = {
    get,
    post,
    put,
    remove
}