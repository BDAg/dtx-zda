const express = require('express');
const path = require('path');
const app = express();
 
app.use(express.static('build-out'));
app.get('/*', function(req,res){
res.sendFile(path.join('build-out', 'index.html'))
});
 
app.listen(process.env.PORT || 8080);