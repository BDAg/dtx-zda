import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api/api.service';

import { Video } from '../../models/video';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  video_list = [];

  constructor(private api: ApiService) {
    this.getVideoList();
   }

  ngOnInit() {
  }

  isAdm(){
    return (localStorage.getItem('USER')) ? JSON.parse(localStorage.getItem('USER'))['cargo'] == 3 : false;
  }

  getVideoList(){
    this.api.getAllVideos()
      .subscribe(
        r => {
          if (r) {
            this.video_list = r
          }
        },
        r => {
          console.error(r.error.error);
        });
  }

}
