import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api/api.service';
import { CustomerService } from '../../auth/customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  nome: string;
  CPF: string;
  Email: string;
  NascimentoData: string = '2000-10-10';
  Senha: string;
  Telefone: string;
  cargo: number = 1;

  CEP: string;
  bairro: string;
  rua: string;
  num_casa: number;
  complemento: string;

  load = false;

  constructor(private api: ApiService, private customer: CustomerService, private router: Router) { }

  ngOnInit() {
  }

  register() {
    this.load = true;

    this.api.postUsuario(
      this.nome,
      this.CPF,
      this.Email,
      this.NascimentoData,
      this.Senha,
      this.Telefone,
      this.cargo
    )
      .subscribe(
        r => {
          if (r.insertId) {

            this.api.login(
              this.Email,
              this.Senha
            )
              .subscribe(
                r_l => {
                  if (r_l.token) {
                    this.customer.setToken(r_l.token);
                    this.customer.setUser(JSON.stringify(r_l.user));
                    
                    this.api.postEnderecos(
                      this.CEP,
                      this.bairro,
                      this.rua,
                      this.num_casa,
                      this.complemento,
                      r.insertId
                    )
                      .subscribe(
                        r_e => {
                          if (r_e.insertId) {
                            this.load = false;
                            this.router.navigate(['']);
                          }
                        },
                        r_e => {
                          this.load = false;
                          alert('Ocorreu um erro!');
                          console.error(r_e);
                        });
                  }
                },
                r_l => {
                  this.load = false;
                  alert('Ocorreu um erro!');
                  console.error(r_l);
                });

          }
        },
        r => {
          this.load = false;
          alert('Ocorreu um erro!');
          console.error(r);
        });
  }

}
