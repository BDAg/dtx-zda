import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api/api.service';
import { CustomerService } from '../../auth/customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  Email = '';
  Senha = '';

  load = false;

  constructor(private api: ApiService, private customer: CustomerService, private router: Router) { }

  ngOnInit() {
  }

  tryLogin() {
    this.load = true;
    this.api.login(
      this.Email,
      this.Senha
    )
      .subscribe(
        r => {
          if (r.token) {
            this.load = false;
            this.customer.setToken(r.token);
            this.customer.setUser(JSON.stringify(r.user));
            this.router.navigate(['']);
          }
        },
        r => {
          this.load = false;
          if (r.status == 500){
            alert('Login ou Senha invalidos!');
          }else{
            alert('Ocorreu um erro!')
          }
          
        });
  }

}
