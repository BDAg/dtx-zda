import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api/api.service';

import { Produtos } from '../../models/produtos';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  product_list = [];

  constructor(private api: ApiService) {
    this.getProductList();
   }

  ngOnInit() {
  }

  isAdm(){
    return (localStorage.getItem('USER')) ? JSON.parse(localStorage.getItem('USER'))['cargo'] == 3 : false;
  }

  getProductList(){
    this.api.getAllProdutos()
      .subscribe(
        r => {
          if (r) {
            this.product_list = r
          }
        },
        r => {
          console.error(r.error.error);
        });
  }

}
