import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-video',
  templateUrl: './register-video.component.html',
  styleUrls: ['./register-video.component.css']
})
export class RegisterVideoComponent implements OnInit {

  nome: string;
  Detalhes: string;

  fileData: File = null;

  load = false;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  fileChange(element) {
    this.fileData = <File>element.target.files[0];
  }


  register() {
    let formData = new FormData();
    formData.append('file', this.fileData);
    this.load = true;
    this.api.uploadImage(formData).subscribe(
      r => {
        this.api.postReceitas(
          this.nome,
          this.Detalhes,
          (r['data']['id']) ? r['data']['id'] : ''
        )
          .subscribe(
            r => {
              if (r.insertId) {
                this.load = false;
                this.router.navigate(['/video']);
              }
            },
            r => {
              this.load = false;
              alert('Ocorreu um erro!');
              console.error(r.error.error);
            });
      },
      r => {
        this.load = false;
        alert('Ocorreu um erro!');
        console.error(r.error.error);
      });

  }

}
