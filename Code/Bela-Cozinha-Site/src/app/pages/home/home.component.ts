import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api/api.service';

import { Produtos } from '../../models/produtos';
import { Video } from '../../models/video';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  video_list = [];
  product_list = [];
  limit = 3;

  constructor(private api: ApiService) { 
    this.getVideoList();
    this.getProductList();
  }

  ngOnInit() {
  }

  getVideoList(){
    this.video_list = [];
    this.api.getAllVideos()
      .subscribe(
        r => {
          if (r) {
            for (let i = 0; i < ((r.length > this.limit) ? this.limit : r.length); i++) {
              this.video_list.push(r[i]);
            }
          }
        },
        r => {
          console.error(r.error.error);
        });
  }

  getProductList(){
    this.product_list = [];
    this.api.getAllProdutos()
      .subscribe(
        r => {
          if (r) {
            for (let i = 0; i < ((r.length > this.limit) ? this.limit : r.length); i++) {
              this.product_list.push(r[i]);
            }
          }
        },
        r => {
          console.error(r.error.error);
        });
  }

}
