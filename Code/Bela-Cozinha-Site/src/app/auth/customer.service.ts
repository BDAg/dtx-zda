import { Injectable } from '@angular/core';

export const TOKEN = 'TOKEN';
export const USER = 'USER';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  setToken(token: string): void {
    localStorage.setItem(TOKEN, token);
  }

  setUser(user: any): void {
    localStorage.setItem(USER, user);
  }


  isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }
}
