import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Login } from '../models/login';
import { Insert } from '../models/insert';
import { Produtos } from '../models/produtos';
import { Video } from '../models/video';

import { HttpHeaders } from '@angular/common/http';
import { TOKEN } from '../auth/customer.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<Login> {
    return this.http.post<Login>('https://api-zda.herokuapp.com/login', {
      Email: email,
      Senha: password
    });
  }


  postUsuario(nome: string,
    CPF: string,
    Email: string,
    NascimentoData: string = '2000-10-10',
    Senha: string,
    Telefone: string,
    cargo: number): Observable<Insert> {
    return this.http.post<Insert>('https://api-zda.herokuapp.com/usuarios', {
      nome: nome,
      CPF: CPF,
      Email: Email,
      NascimentoData: NascimentoData,
      Senha: Senha,
      Telefone: Telefone,
      cargo: cargo
    });
  }

  postEnderecos(CEP: string,
    bairro: string,
    rua: string,
    num_casa: number,
    complemento: string,
    Usuario_idusuario: number): Observable<Insert> {
    return this.http.post<Insert>('https://api-zda.herokuapp.com/enderecos', {
      CEP: CEP,
      bairro: bairro,
      rua: rua,
      num_casa: num_casa,
      complemento: complemento,
      Usuario_idusuario: Usuario_idusuario
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': localStorage.getItem(TOKEN)
      })
    });
  }

  postProdutos(Nome: string,
    preco: number,
    peso: number,
    detalhes: string,
    img:string): Observable<Insert> {
    return this.http.post<Insert>('https://api-zda.herokuapp.com/produtos', {
      Nome: Nome,
      preco: preco,
      peso: peso,
      detalhes: detalhes,
      img:img,
      Usuario_idusuario: JSON.parse(localStorage.getItem('USER'))['idusuario']
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': localStorage.getItem(TOKEN)
      })
    });
  }

  getAllProdutos(): Observable<Array<Produtos>> {
    return this.http.get<Array<Produtos>>('https://api-zda.herokuapp.com/produtos', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': localStorage.getItem(TOKEN)
      })
    });
  }

  getAllVideos(): Observable<Array<Video>> {
    return this.http.get<Array<Video>>('https://api-zda.herokuapp.com/receitas', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': localStorage.getItem(TOKEN)
      })
    });
  }

  uploadImage(formData):Observable<any> {
    return this.http.post('https://api-zda.herokuapp.com/produtos/upload', formData,{
      headers: new HttpHeaders({
        'x-access-token': localStorage.getItem(TOKEN)
      })
    });
  }

  postReceitas(nome: string,
    Detalhes: string,
    video:string): Observable<Insert> {
    return this.http.post<Insert>('https://api-zda.herokuapp.com/receitas', {
      nome: nome,
      Detalhes: Detalhes,
      video:video,
      Usuario_idusuario: JSON.parse(localStorage.getItem('USER'))['idusuario']
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': localStorage.getItem(TOKEN)
      })
    });
  }

}
