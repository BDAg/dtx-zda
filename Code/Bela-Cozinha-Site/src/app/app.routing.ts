import { Routes, RouterModule} from '@angular/router'
import { ModuleWithProviders } from '@angular/core'

import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { CartComponent } from './pages/cart/cart.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ProductComponent } from './pages/product/product.component';
import { VideoComponent } from './pages/video/video.component';
import { RegisterProductsComponent } from './pages/register-products/register-products.component';
import { RegisterVideoComponent } from './pages/register-video/register-video.component';

const APP_ROUTES: Routes = [
    { path: '', component: HomeComponent },
    
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent },
    { path: 'cart', component: CartComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'product', component: ProductComponent },
    { path: 'video', component: VideoComponent },
    { path: 'register-product', component: RegisterProductsComponent },
    { path: 'register-video', component: RegisterVideoComponent },
    { path: '**', pathMatch: 'full', redirectTo: '' },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);