module.exports = {
    userWithChildren: `
        WITH RECURSIVE subusers (id) AS (
            SELECT id FROM users WHERE id = ?
            UNION ALL
            SELECT c.id FROM subusers, users c
                WHERE "parentId" = subusers.id
        )
        SELECT id FROM users
    `
}