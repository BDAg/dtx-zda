module.exports = app => {
    const { existsOrError } = app.api.validation

    const save = (req, res) => {
        const receita = {...req.body}
        
        if(req.params.id) receita.id = req.params.id

        try {
            existsOrError(receita.name, 'Nome não informado')
            existsOrError(receita.content, 'Escreva algo em conteúdo')
            existsOrError(receita.userId, 'Autor não informado')
        } catch(msg) {
            return res.status(400).send(msg)
        }

        if(receita.id) {
            app.db('receitas')
                .update(receita)
                .where({ id: receita.id })
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            app.db('receitas')
                .insert(receita)
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        }
    }

    const remove = async (req, res) => {
        try {
            const rowsDeleted = await app.db('receitas')
                .where({ id: req.params.id }).del()
            
            try {
                existsOrError(rowsDeleted, 'Receita não encontrada')
            } catch(msg) {
                return res.status(400).send(msg)    
            }

            res.status(204).send()
        } catch(msg) {
            res.status(500).send(msg)
        }
    }

    const limit = 10 // usado para paginação

    const get = async (req, res) => {
        const page = req.query.page || 1

        const result = await app.db('receitas').count('id').first()
        const count = parseInt(result.count)

        app.db('receitas')
            .select('id', 'name')
            .limit(limit).offset(page * limit - limit)
            .then(receitas => res.json({ data: receitas, count, limit }))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('receitas')
            .where({ id: req.params.id })
            .first()
            .then(receita => {
                receita.content = receita.content.toString()
                return res.json(receita)
            })
            .catch(err => res.status(500).send(err))
    }

    
    return { save, remove, get, getById }
}