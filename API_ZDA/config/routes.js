const admin = require('./admin')

module.exports = app => {
    app.post('/signup', app.api.user.save)
    app.post('/signin', app.api.auth.signin)
    app.post('/validateToken', app.api.auth.validateToken)

    app.route('/users')
    .all(app.config.passport.authenticate())
    .post(admin(app.api.user.save))
    .get(admin(app.api.user.get))

    app.route('/receitas')
    .all(app.config.passport.authenticate())
    .post(app.api.receita.save)
    .get(app.api.receita.get)

    app.route('/receitas/:id')
    .all(app.config.passport.authenticate())
    .put(app.api.receita.save)
    .get(app.api.receita.getById)
    .delete(app.api.receita.remove)
}