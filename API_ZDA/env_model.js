// Este é um modelo de como deve ser o .env (que não está aqui, obviamente por questão de segurança)

module.exports = {
    authSecret: 'Aqui dentro você coloca qualquer password (senha mestra de token)',
    PORT: '2020'  ,   // Aqui voce coloca a porta do servidor
    db: {
        host : '127.0.0.1',
        port: 5432,
        database: '',
        user: '',
        password: ''
    }
}

