const app = require('express')()
const consign = require('consign')
const db = require('./config/db')
const { PORT } = require('./.env')
app.db = db 

consign()
.include('./config/passport.js')
.then('./config/middlewares.js')
.then('./api/validation.js')
.then('./api')
.then('./config/routes.js')
.into(app)

app.listen(PORT, () => {
    console.log('API RODANDO NA PORTA: ' + PORT)
})