
exports.up = function(knex, Promise) {
    return knex.schema.createTable('addresses', table => {
        table.increments('id').primary()
        table.string('CEP').notNull()
        table.string('Bairro').notNull()
        table.string('Rua').notNull()
        table.integer('numCasa').notNull()
        table.string('complemento', 200)
        table.integer('userId').references('id').inTable('users').notNull()
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('addresses')
};
