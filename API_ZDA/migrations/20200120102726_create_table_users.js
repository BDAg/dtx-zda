
exports.up = function(knex, Promise) {
    return knex.schema.createTable('users', table => {
        table.increments('id').primary()
        table.string('name').notNull()
        table.string('email').notNull().unique()
        table.string('password').notNull()
        table.string('CPF', 11).notNull().unique()
        table.date('dateOfBirth').notNull()
        table.string('phone').notNull()
        table.string('gender').notNull().defaultTo('Não informado')
        table.timestamp('createdAt').defaultTo(knex.fn.now())
        table.timestamp('updateAt').defaultTo(knex.fn.now())
        table.boolean('admin').notNull().defaultTo(false)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users')
};
